const { promisify } = require('util');
const { helloWorld, Client, Topic, PartitionConsumer } = require('./lib');
const { PromisePartitionConsumer } = require('./lib/promise');
console.log(helloWorld());

const client = new Client({
    'bootstrap.servers': 'localhost:9092'
});
console.log(client);
const topic = client.topic("messages", {});
console.log(topic);
const consumer = new PromisePartitionConsumer(topic, 0, 'beginning', 10000);
consumer.consume()
    .then(msg => msg.payload)
    .then(payload => console.log(payload.toString('utf8')));
