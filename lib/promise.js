const { promisify } = require('util');
const { PartitionConsumer } = require('./index.js');

class PromisePartitionConsumer {
    constructor(topic, partition, offset, timeout) {
        this.consumer = new PartitionConsumer(topic, partition, offset);
        this.timeout = timeout;
    }

    consume(batchSize) {
        return promisify(this.consumer.consume).bind(this.consumer)(this.timeout);
    }
}

exports.PromisePartitionConsumer = PromisePartitionConsumer;
