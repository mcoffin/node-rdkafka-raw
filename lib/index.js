const addon = require('../native/index.node');
const { Topic } = addon;

addon.Client.prototype.topic = function topic(topicName, topicConfig = {}) {
    return new Topic(this, topicName, topicConfig);
};

module.exports = addon;
