extern crate neon;
extern crate barebones_rdkafka;

pub use barebones_rdkafka::rdkafka;
pub use barebones_rdkafka::rdkafka_sys;

use barebones_rdkafka::{
    Message,
    OwnedMessage,
};
use barebones_rdkafka::consumer::PartitionConsumer;
use barebones_rdkafka::topic::{
    Topic,
    TopicConfig,
};
use neon::prelude::*;
use neon::{
    class_definition,
    declare_types,
    impl_managed,
    register_module
};
use neon::result::Throw;
use rdkafka::client::{
    Client,
    DefaultClientContext
};
use rdkafka::config::ClientConfig;
use rdkafka::topic_partition_list::Offset;
use rdkafka::types::RDKafkaType;
use std::sync::{
    Arc,
    Mutex,
};
use std::time;

pub struct RDKafkaClient(Arc<Client<DefaultClientContext>>);
pub struct RDKafkaTopic(Arc<Topic<DefaultClientContext, Arc<Client<DefaultClientContext>>>>);
pub struct RDKafkaPartitionConsumer(Arc<Mutex<PartitionConsumer<DefaultClientContext, Arc<Client<DefaultClientContext>>, Arc<Topic<DefaultClientContext, Arc<Client<DefaultClientContext>>>>>>>);

enum OffsetSource<'a> {
    String(Handle<'a, JsString>),
    Number(Handle<'a, JsNumber>),
}

fn offset_from_value<'b, C: Context<'b>>(v: Handle<'b, JsValue>, cx: &mut C) -> Result<Offset, Throw> {
    let source = v.downcast::<JsString>()
        .map(OffsetSource::String)
        .or_else(|_| v.downcast::<JsNumber>().map(OffsetSource::Number))
        .map(Ok)
        .unwrap_or_else(|_| cx.throw_error("offset must be a string or number"))?;
    match source {
        OffsetSource::String(s) => {
            match s.value().as_ref() {
                "beginning" => Ok(Offset::Beginning),
                "end" => Ok(Offset::End),
                s => cx.throw_error(&format!("\"{}\" is not a valid offset.", s)),
            }
        },
        OffsetSource::Number(n) => {
            let n = n.value() as i64;
            Ok(Offset::Offset(n))
        },
    }
}

declare_types! {
    pub class JsClient for RDKafkaClient {
        init(mut cx) {
            let js_config = cx.argument::<JsObject>(0)?;
            let config_keys = js_config.get_own_property_names(&mut cx)
                .and_then(|a| a.to_vec(&mut cx))?;
            let mut client_config = ClientConfig::new();
            for config_key in config_keys.into_iter() {
                let config_key = config_key.downcast_or_throw::<JsString, _>(&mut cx)?;
                let config_value = js_config.get(&mut cx, config_key)
                    .and_then(|v| v.downcast_or_throw::<JsString, _>(&mut cx))?;
                client_config.set(&config_key.value(), &config_value.value());
            }
            let native_client_config = client_config.create_native_config()
                .map(Ok)
                .unwrap_or_else(|e| cx.throw_error(&format!("{:?}", &e)))?;
            Client::new(
                &client_config,
                native_client_config,
                RDKafkaType::RD_KAFKA_CONSUMER,
                DefaultClientContext {}
            )
                .map(|c| RDKafkaClient(Arc::new(c)))
                .map(Ok)
                .unwrap_or_else(|e| cx.throw_error(&format!("{:?}", &e)))
        }
    }
}

declare_types! {
    pub class JsTopic for RDKafkaTopic {
        init(mut cx) {
            use barebones_rdkafka::config::Conf;
            let client = cx.argument::<JsClient>(0)?;
            let topic = cx.argument::<JsString>(1)?;
            let js_config = cx.argument::<JsObject>(2)?;
            let config_keys = js_config.get_own_property_names(&mut cx)
                .and_then(|a| a.to_vec(&mut cx))?;
            let mut topic_config = TopicConfig::new();
            for config_key in config_keys.into_iter() {
                let config_key = config_key.downcast_or_throw::<JsString, _>(&mut cx)?;
                let config_value = js_config.get(&mut cx, config_key)
                    .and_then(|v| v.downcast_or_throw::<JsString, _>(&mut cx))?;
                let _set_res = topic_config.set(&config_key.value(), &config_value.value());
            }
            let client = {
                let guard = cx.lock();
                let client = client.borrow(&guard);
                client.0.clone()
            };
            Ok(Topic::new(client, &topic.value(), topic_config))
                .map(Arc::new)
                .map(|t| RDKafkaTopic(t))
        }
    }
}

struct MessageWrapper(OwnedMessage);

unsafe impl Send for MessageWrapper {}

struct ConsumeTask {
    consumer: Arc<Mutex<PartitionConsumer<DefaultClientContext, Arc<Client<DefaultClientContext>>, Arc<Topic<DefaultClientContext, Arc<Client<DefaultClientContext>>>>>>>,
    timeout: time::Duration,
}

unsafe impl Send for ConsumeTask {}

fn message_to_object<'c, M: Message, C: Context<'c>>(msg: &M, ctx: &mut C) -> Result<Handle<'c, JsObject>, Throw> {
    use std::slice;
    let obj = ctx.empty_object();
    if let Some(payload) = msg.payload() {
        let mut buffer = JsBuffer::new(ctx, payload.len() as u32)?;
        {
            let guard = ctx.lock();
            let buffer = buffer.borrow_mut(&guard).as_mut_slice();
            buffer.copy_from_slice(payload);
        }
        obj.set(ctx, "payload", buffer)?;
    }
    if let Some(key) = msg.key() {
        let mut buffer = JsBuffer::new(ctx, key.len() as u32)?;
        {
            let guard = ctx.lock();
            let buffer = buffer.borrow_mut(&guard).as_mut_slice();
            buffer.copy_from_slice(key);
        }
        obj.set(ctx, "key", buffer)?;
    }
    Ok(obj)
}

impl Task for ConsumeTask {
    type Output = Option<MessageWrapper>;
    type Error = String;
    type JsEvent = JsValue;

    fn perform(&self) -> Result<Self::Output, Self::Error> {
        let mut consumer = self.consumer.lock().unwrap();
        consumer.consume(self.timeout)
            .map(|v| v.map(MessageWrapper))
            .map_err(|e| format!("{:?}", &e))
    }

    fn complete(self, mut cx: TaskContext, result: Result<Self::Output, Self::Error>) -> JsResult<JsValue> {
        use barebones_rdkafka::Message;
        match result {
            Ok(Some(msg)) => {
                let msg: &rdkafka_sys::rd_kafka_message_t = &msg.0;
                let msg = message_to_object(msg, &mut cx)?;
                Ok(msg.upcast::<JsValue>())
            },
            Ok(None) => Ok(cx.null().upcast::<JsValue>()),
            Err(e) => cx.throw_error(&e),
        }
    }
}

declare_types! {
    pub class JsPartitionConsumer for RDKafkaPartitionConsumer {
        init(mut cx) {
            let topic = {
                let js_topic = cx.argument::<JsTopic>(0)?;
                let guard = cx.lock();
                let topic = js_topic.borrow(&guard);
                topic.0.clone()
            };
            let partition = cx.argument::<JsNumber>(1)
                .map(|v| v.value())
                .map(|v| v as i32)?;
            let offset = cx.argument::<JsValue>(2)
                .and_then(|v| offset_from_value(v, &mut cx))?;
            let consumer = PartitionConsumer::new(topic, partition, offset)
                .map_err(|e| format!("Error creating PartitionConsumer: {:?}", &e))
                .map(Ok)
                .unwrap_or_else(|e| cx.throw_error(&e))?;
            Ok(RDKafkaPartitionConsumer(Arc::new(Mutex::new(consumer))))
        }

        method consume(mut cx) {
            let timeout = cx.argument::<JsNumber>(0)
                .map(|v| v.value() as u64)
                .map(time::Duration::from_millis)?;
            let cb = cx.argument::<JsFunction>(1)?;
            let this = cx.this();
            let consumer = {
                let guard = cx.lock();
                let consumer = this.borrow(&guard);
                consumer.0.clone()
            };
            let consume_task = ConsumeTask {
                consumer: consumer,
                timeout: timeout,
            };
            consume_task.schedule(cb);
            Ok(cx.undefined().upcast::<JsValue>())
        }
    }
}

fn hello_world(mut cx: FunctionContext) -> JsResult<JsString> {
    Ok(cx.string("Hello, world!"))
}

register_module!(mut m, {
    m.export_function("helloWorld", hello_world)?;
    m.export_class::<JsClient>("Client")?;
    m.export_class::<JsTopic>("Topic")?;
    m.export_class::<JsPartitionConsumer>("PartitionConsumer")
});
